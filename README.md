# [Inzidenzgenerator](https://heroldini.gitlab.io/inzidenzgenerator)
* Mit [diesem Tool](https://heroldini.gitlab.io/inzidenzgenerator) kann das Testgeschehen der Coronapandemie nachgerechnet werden.
* Genauere Anleitung im Tool zu finden.

## Technisches
* Das Tool ist eine HTML-Seite für den Browser.
* Die Berechnungen laufen Clientseitig per JavaScript im Browser ab.
* CSS übernimmt die Optik.

Es ist möglich das Projekt auf dem eigenen PC lokal zu benutzen.
* Dazu einfach die index.html mit dem gewünschten Browser öffnen.
* Die Berechnungen der beiden Rechner sind in jeweils eine .js - Datei ausgelagert und voll auskommentiert (englisch)

## Links zum Tool
* [Onlineansicht](https://heroldini.gitlab.io/inzidenzgenerator)
* [Download](https://gitlab.com/heroldini/inzidenzgenerator/-/archive/master/inzidenzgenerator-master.zip)

## Lizenz
* [CC0](https://creativecommons.org/publicdomain/zero/1.0)
* Darf frei für jeden Zweck verwendet werden, es wird keine Haftung übernommen!
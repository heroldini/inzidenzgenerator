// ---------------------------------------------------------------------------------
// date: 09 feb 2021
// licence: CC0: https://creativecommons.org/publicdomain/zero/1.0
// description: This Script calculates the german "Inzidenz" (incidence) for the corona lockdown.
//              It is possible zu calculate backwards to get the number of tests you
//              need to take on the population to get these numbers.
//              
//              The "Inzidenz" value is the number which is used from the goverment
//              to lockdown citys, countys or the whole country!
// ---------------------------------------------------------------------------------

// --------------------------------------------------------------------------------- CONSTANTS & GLOBAL VARS
var INIT_INFECTIONS;                                                                // INIT value of infections
var INIT_RESIDENTS;                                                                 // INIT value of residents
var INZIDENZ_BASE;                                                                  // INIT value of incidence base

var oInputInfections                                                                // Contains object of inputbox "Infektionen"
var oInputResidents                                                                 // Contains object of inputbox "Einwohner"
var oInputIncidenceBase                                                             // Contains object of inputbox "Inzidenzbasis"
var oInputIncidence                                                                 // Contains object of inputbox "Inzidenz"




// --------------------------------------------------------------------------------- INIT
function InitIncidence()
{
  // INIT VALUES
  INIT_INFECTIONS = 750;                                                            // INIT value of infections
  INIT_RESIDENTS = 1500000;                                                         // INIT value of residents
  INZIDENZ_BASE = 100000;                                                           // INIT value of incidence base
  
  
  // HTML objects
  oInputInfections = document.getElementById("fInfections");                        // Inputbox "Infektionen"
  oInputResidents = document.getElementById("fResidents");                          // Inputbox "Einwohner"
  oInputIncidenceBase = document.getElementById("fIncidenceBase");                  // Inputbox "Inzidenzbasis"
  oInputIncidence = document.getElementById("fIncidence");                          // Inputbox "Inzidenz"
  
  
  // Fill init values
  oInputInfections.value = INIT_INFECTIONS;                                         // write infection value
  oInputResidents.value = INIT_RESIDENTS;                                           // write residents value
  oInputIncidenceBase.value = INZIDENZ_BASE;                                        // write incidence base value
  
  // Calculate incidence value
  GetIncidenceFromInceftions();                                                     // Calculate incidence
}




// --------------------------------------------------------------------------------- FUNCTIONS (HTML ON-CHANGE EVENT METHODES)
// EVENT inputbox fInfections
function On_fInfections()
{
  GetIncidenceFromInceftions();                                                     // Calculate incidence
}



// EVENT inputbox fResidents
function On_fResidents()
{
  GetIncidenceFromInceftions();                                                     // Calculate incidence
  GetInceftionsFromIncidence();                                                     // Calculate needed infection numbers
}



// EVENT inputbox fIncidenceBase
function On_fIncidenceBase()
{
  GetIncidenceFromInceftions();                                                     // Calculate incidence
  GetInceftionsFromIncidence();                                                     // Calculate needed infection numbers
}



// EVENT inputbox fIncidence
function On_fIncidence()
{
  GetInceftionsFromIncidence();                                                     // Calculate needed infection numbers
}




// --------------------------------------------------------------------------------- FUNCTIONS (SHORT VALUE CALC CALLS)
// Calculate incidence inputbox from incidence values
function GetIncidenceFromInceftions()
{
  // calculate incidence value
  var fResIncidence = CalcIncidence(oInputInfections.value, oInputResidents.value, oInputIncidenceBase.value);
  oInputIncidence.value = Math.round(fResIncidence);                                // write incidence value (rounded)
}



// Calculate infections inputbox from incidence values
function GetInceftionsFromIncidence()
{
  // calculate "needed" infections
  var fResInfections = CalcInfections(oInputIncidence.value, oInputResidents.value, oInputIncidenceBase.value);
  oInputInfections.value = Math.round(fResInfections);                              // Write infections (rounded)
}




// --------------------------------------------------------------------------------- FUNCTIONS (FORMULAS)
// Calculates the incidence value
// Returns the incidence value
//      f_Infections              FLOAT     Infektions (e.g. sum of the last 7 days)
//      f_Residents               FLOAT     Number of residents of an city/county/state
//      f_InzidenzBase            FLOAT     The incidence base number (e.g. germany standard 100.000)
function CalcIncidence(f_Infections, f_Residents, f_InzidenzBase)
{
  var InfectPerResidents = f_Infections / f_Residents;                              // Infection "per 1 resident"
  return InfectPerResidents * f_InzidenzBase;                                       // Incidence value
}



// Calculates the needed infections to get the given incidence value
// Returns the "needed" infections
//      f_Infections              FLOAT     Infektionen (z.b. Summe der letzten 7 Tage um eine 7-Tages-Inzidenz zu berechnen)
//      f_Residents               FLOAT     Anzahl der Einwohner der Stadt
//      f_InzidenzBase            FLOAT     Basis/Vergleichszahl auf die der Inzidenz hochgerechnet werden soll (Standart 100.000)
function CalcInfections(f_Incidence, f_Residents, f_InzidenzBase)
{
  var InfectPerResidents = f_Incidence / f_InzidenzBase;                            // Infection "per 1 resident"
  return InfectPerResidents * f_Residents;                                          // "Needed" infections
}

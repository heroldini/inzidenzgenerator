// ---------------------------------------------------------------------------------
// date: 09 feb 2021
// licence: CC0: https://creativecommons.org/publicdomain/zero/1.0
// description: This Script calculates the german "Inzidenz" (incidence) for the corona lockdown.
//              It is possible zu calculate backwards to get the number of tests you
//              need to take on the population to get these numbers.
//              
//              The "Inzidenz" value is the number which is used from the goverment
//              to lockdown citys, countys or the whole country!
// ---------------------------------------------------------------------------------


// --------------------------------------------------------------------------------- CONSTANTS & GLOBAL VARS
var INIT_TESTS;                                                                     // Init value of numbers of tests
var INIT_SICK;                                                                      // Init value of numbers of sick people
var INIT_SENS;                                                                      // Init value of sensitivity of the test in %
var INIT_SPEC;                                                                      // Init value of specificity of the test in %

var oInputTests                                                                     // Contains object of inputbox "fTests"
var oInputSick                                                                      // Contains object of inputbox "fSick"
var oInputSens                                                                      // Contains object of inputbox "fSens"
var oInputSpec                                                                      // Contains object of inputbox "fSpec"
var oInputNeg                                                                       // Contains object of inputbox "fNeg"
var oInputWrongNeg                                                                  // Contains object of inputbox "fWrongNeg"
var oInputPos                                                                       // Contains object of inputbox "fPos"
var oInputWrongPos                                                                  // Contains object of inputbox "fWrongPos"




// --------------------------------------------------------------------------------- INIT
function InitTests()
{
  // INIT VALUES
  INIT_TESTS = 150000;                                                              // Init value of numbers of tests
  INIT_SICK = 0;                                                                    // Init value of numbers of sick people
  INIT_SENS = 99.0;                                                                 // Init value of sensitivity of the test in %
  INIT_SPEC = 99.5;                                                                 // Init value of specificity of the test in %
  
  
  // HTML objects
  oInputTests = document.getElementById("fTests");                                  // Inputbox "fTests"
  oInputSick = document.getElementById("fSick");                                    // Inputbox "fSick"
  oInputSens = document.getElementById("fSens");                                    // Inputbox "fSens"
  oInputSpec = document.getElementById("fSpec");                                    // Inputbox "fSpec"
  oInputNeg = document.getElementById("fNeg");                                      // Inputbox "fNeg"
  oInputWrongNeg = document.getElementById("fWrongNeg");                            // Inputbox "fWrongNeg"
  oInputPos = document.getElementById("fPos");                                      // Inputbox "fPos"
  oInputWrongPos = document.getElementById("fWrongPos");                            // Inputbox "fWrongPos"
  
  
  // Fill init values
  oInputTests.value = INIT_TESTS;                                                   // write numbers of tests
  oInputSick.value = INIT_SICK;                                                     // write numbers of sick people
  oInputSens.value = INIT_SENS;                                                     // write sensitivity value
  oInputSpec.value = INIT_SPEC;                                                     // write specificity value
  
  
  // Calculate test results of the mass testing
  GetAllNegativ();                                                                  // Calculate all negativ test results
  GetFalseNegativ();                                                                // Calculate false negative test results
  
  GetAllPositiv();                                                                  // Calculate all positive test results
  GetFalsePositiv();                                                                // Calculate false positive test results
}




// --------------------------------------------------------------------------------- FUNCTIONS (HTML ON-CHANGE EVENT METHODES)
// EVENT inputbox fTests
function On_fTests()
{
  GetAllNegativ();                                                                  // Calculate all negativ test results
  GetFalseNegativ();                                                                // Calculate false negative test results
  
  GetAllPositiv();                                                                  // Calculate all positive test results
  GetFalsePositiv();                                                                // Calculate false positive test results
}



// EVENT inputbox fSick
function On_fSick()
{
  GetAllNegativ();                                                                  // Calculate all negativ test results
  GetFalseNegativ();                                                                // Calculate false negative test results
  
  GetAllPositiv();                                                                  // Calculate all positive test results
  GetFalsePositiv();                                                                // Calculate false positive test results
}



// EVENT inputbox fSens
function On_fSens()
{
  GetAllNegativ();                                                                  // Calculate all negativ test results
  GetFalseNegativ();                                                                // Calculate false negative test results
  
  GetAllPositiv();                                                                  // Calculate all positive test results
  GetFalsePositiv();                                                                // Calculate false positive test results
}



// EVENT inputbox fSpec
function On_fSpec()
{
  GetAllNegativ();                                                                  // Calculate all negativ test results
  GetFalseNegativ();                                                                // Calculate false negative test results
  
  GetAllPositiv();                                                                  // Calculate all positive test results
  GetFalsePositiv();                                                                // Calculate false positive test results
}



// EVENT inputbox fNeg
function On_fNeg()
{
}



// EVENT inputbox fWrongNeg
function On_fWrongNeg()
{
}



// EVENT inputbox fPos
function On_fPos()
{
  GetNeededTests();
}



// EVENT inputbox fWrongPos
function On_fWrongPos()
{
}




// --------------------------------------------------------------------------------- FUNCTIONS (SHORT VALUE CALC CALLS)
// Calculate all-negativ from test
function GetAllNegativ()
{
  // Calculate false negativ + detected real negative
  var iAllNeg = CalcAllNeg(oInputTests.value, oInputSick.value, oInputSens.value, oInputSpec.value);
  oInputNeg.value = iAllNeg;                                                        // Write negativ tests
}



// Calculate false negativ for inputbox
function GetFalseNegativ()
{
  // Calculate false negativ
  var iFalseNeg = CalcFalseNeg(oInputTests.value, oInputSick.value, oInputSens.value);
  oInputWrongNeg.value = iFalseNeg;                                                 // Write false negativ tests
}



// Calculate all-positiv from test
function GetAllPositiv()
{
  // Calculate false positive + detected real positive
  var iAllPos = CalcAllPos(oInputTests.value, oInputSick.value, oInputSens.value, oInputSpec.value);
  oInputPos.value = iAllPos;                                                        // Write positiv tests
}



// Calculate false positiv for inputbox
function GetFalsePositiv()
{
  // Calculate false positiv
  var iFalsePos = CalcFalsePos(oInputTests.value, oInputSick.value, oInputSpec.value);
  oInputWrongPos.value = iFalsePos;                                                 // Write false positiv tests
}



// Calculate required numbers of tests to get a given (false-)positiv result
function GetNeededTests()
{
  oInputSick.value = 0;                                                             // Set sick people to zero
  oInputTests.value = CalcNeededTests(oInputPos.value, oInputSpec.value);           // Calculate required numbers of tests
  
  // Recalculate the results
  GetAllNegativ();                                                                  // Calculate all negativ test results
  GetFalseNegativ();                                                                // Calculate false negative test results
  
  GetAllPositiv();                                                                  // Calculate all positive test results
  GetFalsePositiv();                                                                // Calculate false positive test results
}




// --------------------------------------------------------------------------------- FUNCTIONS (FORMULAS)
// Calculate the right positiv numbers of the mass testing                [How many sick persons are detected RIGHT-POSITIVE          / 100 * f_Sensitivity   ]
// Return the right positiv number (round to full people)
//      f_Sick                    FLOAT     Number of real sick people
//      f_Sensitivity             FLOAT     Sensitivity: How many right-positiv results of the test in [%]
function CalcRightPostiv(f_Sick, f_Sensitivity)
{
  var fRightPos = f_Sick / 100 * f_Sensitivity;                                     // Calculate the right positives
  return Math.round(fRightPos);                                                     // Round to full peoples, return
}



// Calculate the right negativ numbers of the mass testing                [How many healthy persons are detected RIGHT-NEGATIVE       / 100 * f_Specificity   ]
// Return the right negativ number (round to full people)
//      f_Tests                   FLOAT     Number of tested people
//      f_Sick                    FLOAT     Number of real sick people
//      f_Specificity             FLOAT     Specificity: How many right-negativ results of the test in [%]
function CalcRightNegativ(f_Tests, f_Sick, f_Specificity)
{
  var fHealthy = f_Tests - f_Sick;                                                  // Calculate the healthy people
  var fRightNeg = fHealthy / 100 * f_Specificity;                                   // Calculate the right negatives
  return Math.round(fRightNeg);                                                     // Round to full peoples, return
}



// Calculate the false negativ numbers of the mass testing                [How many sick persons are detected FALSE-NEGATIVE          / 100 * (100-f_Sensitivity)   ]
// Return the false negativ numver (round to full people)
//      f_Tests                   FLOAT     Number of tested people
//      f_Sick                    FLOAT     Number of real sick people
//      f_Sensitivity             FLOAT     Sensitivity: How many right-positiv results of the test in [%]
function CalcFalseNeg(f_Tests, f_Sick, f_Sensitivity)
{
  var fFalseNeg = f_Sick / 100 * (100-f_Sensitivity);                               // Calculate the false negatives
  return Math.round(fFalseNeg);                                                     // Round to full peoples, return
}



// Calculate the false positiv numbers of the mass testing                [How many healthy persons are detected FALSE-POSITIVE       / 100 * (100-f_Specificity)   ]
// Return the false positiv number (round to full people)
//      f_Tests                   FLOAT     Number of tested people
//      f_Sick                    FLOAT     Number of real sick people
//      f_Specificity             FLOAT     Specificity: How many right-negativ results of the test in [%]
function CalcFalsePos(f_Tests, f_Sick, f_Specificity)
{
  var fHealthy = f_Tests - f_Sick;                                                  // Calculate the healthy people
  var fFalsePos = fHealthy / 100 * (100-f_Specificity);                             // Calculate the false positives
  return Math.round(fFalsePos);                                                     // Round to full peoples, return
}



// Calculate the negativ people of the mass testings (false + right negativ detected)
// Returns negativ numbers of the testings (false + right negativ detected)
//      f_Tests                   FLOAT     Number of tested people
//      f_Sick                    FLOAT     Number of real sick people
//      f_Sensitivity             FLOAT     Sensitivity: How many right-positiv results of the test in [%]
//      f_Specificity             FLOAT     Specificity: How many right-negativ results of the test in [%]
function CalcAllNeg(f_Tests, f_Sick, f_Sensitivity, f_Specificity)
{
  var fRightNeg = CalcRightNegativ(f_Tests, f_Sick, f_Specificity);                 // Calculate the right negatives
  var fFalseNeg = CalcFalseNeg(f_Tests, f_Sick, f_Sensitivity);                     // Calculate the false negatives
  var fAllNeg = fRightNeg + fFalseNeg;                                              // Sum of negativ results
  return Math.round(fAllNeg);                                                       // Round to full peoples, return
}



// Calculates the positiv people of the mass testings (false + right postive detected)
// Returns positiv numbers of the testings (false + right postive detected)
//      f_Tests                   FLOAT     Number of tested people
//      f_Sick                    FLOAT     Number of real sick people
//      f_Sensitivity             FLOAT     Sensitivity: How many right-positiv results of the test in [%]
//      f_Specificity             FLOAT     Specificity: How many right-negativ results of the test in [%]
function CalcAllPos(f_Tests, f_Sick, f_Sensitivity, f_Specificity)
{
  var fRightPos = CalcRightPostiv(f_Sick, f_Sensitivity);                           // Calculate the right positives
  var fFalsePos = CalcFalsePos(f_Tests, f_Sick, f_Specificity);                     // Calculate the false positives
  var fAllPos = fRightPos + fFalsePos;                                              // Sum of positiv results
  return Math.round(fAllPos);                                                       // Round to full peoples, return
}



// Calculates the needed tests to get (false-)positive result numbers
// Return the min. needed tests (rounded up to full people)
//      f_FalsePos                FLOAT     Number of false positiv tests
//      f_Specificity             FLOAT     Specificity: How many right-negativ results of the test in [%]
function CalcNeededTests(f_FalsePos, f_Specificity)
{
  var fTestsFromHealthy = f_FalsePos / (100-f_Specificity) * 100;                   // Calculate needed tests to get the given false positiv results from test-specificity
  var iRound = Math.round(fTestsFromHealthy);                                       // Calculated (float) number rounded up or down
  
  if (iRound < fTestsFromHealthy)                                                   // If calculated (float) number is rounded down...
    return iRound + 1;                                                              // Return rounded number +1 (=round up)
  
  return iRound;                                                                    // Else: Return rounded (up) number
}
